#!/bin/bash


#export PATH=/home/$USER/.config/npm-global/bin:$PATH
#export PATH=/home/$USER/Android/Sdk/platform-tools:$PATH

#npm
NPM_PACKAGES="${HOME}/.npm-packages"

export PATH="$NPM_PACKAGES/bin:$PATH"

# Unset manpath so we can inherit from /etc/manpath via the `manpath` command
unset MANPATH # delete if you already modified MANPATH elsewhere in your config
export MANPATH="$NPM_PACKAGES/share/man:$(manpath)"

export ANDROID_ROOT="/home/kalnar/Android/Sdk/platform-tools"

export PATH="$ANDROID_ROOT:$PATH"
