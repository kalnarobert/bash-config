#!/bin/bash

__cdrecursively__() {
  if [[ "$1" == "-up" ]]; then
    dir=`find ../../ -maxdepth 2 -type d -not -path '*/\.[a-zA-Z0-9]*' | fzf --height 15 --preview "ls {}"`
  else
    dir=`find -maxdepth 2 -type d -not -path '*/\.*' | fzf --height 15 --preview "ls {}"`
  fi
  cd $dir
  echo "in $PWD..."
  echo "to quit, press enter..."
  IFS= read -n 1 -s key
  if [[ "${key}" == "" ]]; then
    echo "in $PWD..."
    return 1
  fi
  if [[ "${key}" == "u" ]]; then
    __cdrecursively__ -up
  else
    __cdrecursively__
  fi
}

greppingToClipboard() {
  IFS=$'\r\n'

  select d in $(grep -h --color "$@" | awk '!x[$0]++'); do
    if [ -n "$d" ]; then
      # put your command here
      echo "Copied to clipboard: $d"
      printf "$d" | xsel --clipboard
      break
    fi
  done
}

gototp() {
  IFS=$'\r\n'
  select sujet in `cat /home/$USER/pCloud/config/bash/sujet.txt`; do
    if [ -n "$sujet" ]; then
      cd /home/$USER/pCloud/university/lyon1/m1.informatique/semester1/${sujet}/tp
      break
    fi
  done
}
gototd() {
  IFS=$'\r\n'
  select sujet in `cat /home/$USER/pCloud/config/bash/sujet.txt`; do
    if [ -n "$sujet" ]; then
      cd /home/$USER/pCloud/university/lyon1/m1.informatique/semester1/${sujet}/td
      break
    fi
  done
}
gotocm() {
  IFS=$'\r\n'
  select sujet in `cat /home/$USER/pCloud/config/bash/sujet.txt`; do
    if [ -n "$sujet" ]; then
      cd /home/$USER/pCloud/university/lyon1/m1.informatique/semester1/${sujet}/cm
      break
    fi
  done
}

setDate() {
  exiv2 -M"set Exif.Image.DateTime $2 00:00:00" $1
  exiv2 -M"set Exif.Photo.DateTimeOriginal $2 00:00:00" $1
  exiv2 -M"set Exif.Photo.DateTimeDigitized $2 00:00:00" $1
}

setGPS() {
  exiv2 -M"set Exif.GPSInfo.GPSLatitude $1/1000000 0/1 0/1" -M"set Exif.GPSInfo.GPSLatitudeRef N" -M"set Exif.GPSInfo.GPSLongitude $2/1000000 0/1 0/1" -M"set Exif.GPSInfo.GPSLongitudeRef E" $3
}

styluslabs() {
  ${BACKED_UP_HOME}software/linux/write.linux/Write/Write $1
}


getFullPathOf() {
  kopi `readlink -f $1`
}

pandocConvert() {
  pandoc -s ${1%.*}.md -o ${1%.*}.html --css=file:///home/$USER/config/markdown/style/pandoc.css --standalone --mathjax=file:///home/$USER/softwares/MathJax-master/MathJax.js?config=TeX-MML-AM_CHTML-full,local/local --toc --toc-depth=2
  #pandoc -s ${1%.*}.md -o ${1%.*}.html --css=file:///home/$USER/config/pandoc/pandoc-config/style/github-grip.css --standalone --mathjax=file:///home/$USER/softwares/MathJax-master/MathJax.js?config=TeX-MML-AM_CHTML-full,local/local --toc --toc-depth=2
}

vicd() {
  local dst="$(command vifm --choose-dir -)"
  if [ -z "$dst" ]; then
    echo 'Directory picking cancelled/failed'
    return 1
  fi
  cd "$dst"
}
 
function show_path(){
    old=$IFS
    IFS=:
    printf "%s\n" $PATH
    IFS=$old
}



function gpp() {
  g++ -o ${1%.cpp} ${1}
}


function set_session_home() {
  if [ "${1}" != "" ];then
    CURRENT_TMX_SESSION=$(tmux display-message -p '#S')
    tmux setenv "HOME_${CURRENT_TMX_SESSION}" "${1}"
  else
    CURRENT_TMX_SESSION=$(tmux display-message -p '#S')
    tmux setenv "HOME_${CURRENT_TMX_SESSION}" `pwd`
  fi
}

function go_to_session_home() {
  CURRENT_TMX_SESSION=$(tmux display-message -p '#S')
  SESSION_HOME=$(tmux showenv "HOME_${CURRENT_TMX_SESSION}" | sed s'/.*=//')
  cd "${SESSION_HOME}"
}

alias sd=go_to_session_home


function calen() {
  if [[ "${1}" == "" ]]; then
    gcal --starting-day=1 -q fr -n .
  else 
    gcal --starting-day=1 -q fr -n $@
  fi
}
