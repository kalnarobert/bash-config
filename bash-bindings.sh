#!/bin/bash

# bind Alt+Tab to fzf finder
bind -x '"\e\t":"fzf-file-widget"'

# grep file contents
__ggrep__() {
  grep --line-buffered --color=never -r "" * | fzf
}
bind '"\eo":"\C-x\C-addi`__ggrep__`\C-x\C-e\C-x\C-r\C-x^\C-x\C-a$a"'
bind -m vi-command '"\eo":"i\eo"'

# grep history of all commands
__ghist__() {
  cat ${BACKED_UP_HOME}/history/${PC_ENV:-laptop}/20* | awk '!x[$0]++' | fzf
}
bind '"\C-h":"\C-x\C-addi`__ghist__`\C-x\C-e\C-x\C-r\C-x^\C-x\C-a$a"'
bind -m vi-command '"\C-h":"i\C-h"'

# recursive cd
bind -x '"\C-n":"__cdrecursively__"'

# grep history of all commands
__gotobookmark__() {
  dir=`cat ${BACKED_UP_HOME}/config/bash/path_bookmarks | awk '!x[$0]++' | fzf` 
  echo "cd ${dir}"
}
bind '"\C-b":"\C-x\C-addi`__gotobookmark__`\C-x\C-e\C-x\C-r\C-x^\C-x\C-a$a"'
bind -m vi-command '"\C-b":"i\C-b"'

__favorite_commands__() {
  COM=$(cat ${BACKED_UP_HOME}/config/bash/fav_commands | awk '!x[$0]++' | fzf)
  echo "$COM"
}
bind '"\C-f":"\C-x\C-addi`__favorite_commands__`\C-x\C-e\C-x\C-r\C-x^\C-x\C-a$a"'
bind -m vi-command '"\C-f":"i\C-f"'

__gitco_fuzzy__() {
  branch=$(git for-each-ref --format='%(refname:short)' refs/heads | fzf)
  echo "git checkout $branch"
}
bind '"\C-k":"\C-x\C-addi`__gitco_fuzzy__`\C-x\C-e\C-x\C-r\C-x^\C-x\C-a$a"'
bind -m vi-command '"\C-k":"i\C-k"'
